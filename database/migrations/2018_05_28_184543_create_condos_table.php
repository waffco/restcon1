<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('num_apts');
            $table->string('type');
            $table->string('addr_street');
            $table->string('addr_num');
            $table->string('addr_city');
            $table->string('addr_zip');
            $table->string('addr_muni');
            $table->string('addr_state');
            $table->string('addr_country');
            $table->integer('condo_level')->default(0);
            $table->string('rulebook')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_clabe')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condos');
    }
}
