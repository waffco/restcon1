<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('condo_id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique()->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('interior')->unique();
            $table->rememberToken();
            $table->integer('account_level')->default(0);
            $table->char('sex')->nullable();
            $table->timestamp('birthdate')->useCurrent();
            $table->string('tel1')->nullable();
            $table->string('tel2')->nullable();
            $table->boolean('newsletter')->default(0);
            $table->boolean('active')->default(0);
            $table->double('quota')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
