<?php

/* The aim of this class is to provide a re-usable method of applying common
 * (and specific) url params across all queryable API resources. 
 */  

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class ParamHelper
{
    public function processParams(Request $request, Builder $query, $resource){
        
        // get those URL params
        $params = $request->query();
        //dd($params);
        
        /*********************************
        * begin extra param manipulation *
        *********************************/

        // if limit param is present, limit results to that value
        if(!empty($params['limit'])){
            $query = $query->limit($params['limit']);
        }

        // if fullInfo param is present, add more fields to response (value irrelevant)
        if(!empty($params['fullInfo'])){
            
            // Naturally the fields added depend on the resource being dealt with...
            switch($resource){
                
                case "product":
                    $query = $query->addSelect(
                        'created_at', 
                        'updated_at', 
                        'availability'
                        );
                    break;
                
                case "admin":
                    $query = $query->addSelect(
                        'created_at', 
                        'updated_at', 
                        'account_level',
                        'sex',
                        'birthdate',
                        'tel1',
                        'tel2',
                        'addr_street',
                        'addr_num_ext',
                        'addr_num_int',
                        'city',
                        'state',
                        'zip',
                        'newsletter',
                        'active'
                        );
                    $query = $query->with('condos.residents');
                    break;
                
                case "condo":
                    $query = $query->addSelect(
                        'created_at', 
                        'updated_at',
                        'addr_street',
                        'addr_num',
                        'addr_city',
                        'addr_state',
                        'addr_zip',
                        'addr_muni',
                        'addr_country',
                        'condo_level',
                        'rulebook',
                        'bank_name',
                        'bank_account',
                        'bank_clabe'
                        );
                    $query = $query->with(['admins','residents']);
                    break;
                
                case "resident":
                    $query = $query->addSelect(
                        'created_at', 
                        'updated_at',
                        'email',
                        'account_level',
                        'sex',
                        'birthdate',
                        'tel1',
                        'tel2',
                        'newsletter',
                        'active'
                        );
                    $query = $query->with('condos.admins');
                    break;
            }
 
        }

        // if order param is present, sort in that order
        if(!empty($params['order'])){
            $fieldName = 'id';
            $order = $params['order'];

            // if sortField param is present, sort on that field name
            if(!empty($params['sortField'])){
                $fieldName = $params['sortField'];
            }

            $query = $query->orderBy($fieldName,$order);
        }

        // now return the results

        // if pagination was requested....
        if(!empty($params['paginate'])){
            return $query->paginate($params['paginate']);
        }
        // otherwise return the full set.
        else{ 
            // run and return custom query
            return $query->get();
        }
        
    }
}
