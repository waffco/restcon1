<?php

namespace App\Http\Controllers;

use App\Resident;
use App\Condo;
use Illuminate\Http\Request;
use Hash;
use App\ParamHelper;

class ResidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // prepare a vanilla query that returns simple info
        $query = Resident::select('id','condo_id','fname','lname','username', 'interior');
        
        // get the available url params
        $params = $request->query();
        
        // if no url params
        if(empty($params)){
            
            // return all results of the vanilla query
            return $query->get();
            
        }
        
        // otherwise...
        else{
            
            // make a new paramHelper...
            $helper = new ParamHelper();
            
            // ..to process them params, modify query, and return results.
            return $helper->processParams($request,$query,'resident');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function show(Resident $resident)
    {
        return response()->json($resident, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'condo_id' => 'required',
          'fname' => 'required',
          'lname' => 'required',
          'email' => 'unique:residents|email',
          'username' => 'required|unique:residents',
          'password' => 'required|alpha_num',
          'interior' => 'required|unique:residents',
        ]);

        //$pass = Hash::make($request->password);
        //$request->password = $pass;
        
        /*$resident = Resident::create([
            'fname' => $request->fname, 
            'lname' => $request->lname, 
            'email' => $request->email, 
            'password' => Hash::make($request->password)]);*/
        
        $condo = Condo::find($request->condo_id);
        
        if($condo){
            $resident = Resident::create([
                'condo_id' => $request->condo_id,
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'interior' => $request->interior,
              ]);
            $resident->condos()->associate($request->condo_id);
            $resident->save();
            return response()->json($resident, 201);
        }
        else{
            return response()->json(["success"=>false,"message"=>"provided condo id does not exist, unable to register resident"], 422);
        }
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resident $resident)
    {
        $this->validate($request, [
          'email' => 'unique:residents|email',
          'username' => 'unique:residents',
          'interior' => 'unique:residents',
        ]);
        
        $resident->update($request->all());
 
        return response()->json($resident, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resident $resident)
    {
        $resident->delete();
 
        return response()->json(['success'=> true, 'message'=> 'Resident #'.$resident->id.' has been succesfully deleted'], 200);
    }
}
