<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Product;
use App\ParamHelper; 

class ProductsController extends Controller
{
 
    public function index(Request $request)
    {
        // prepare a vanilla query that returns simple info
        $query = Product::select('id','title','description','price');
        
        // get the available url params
        $params = $request->query();
        
        // if no url params
        if(empty($params)){
            
            // return all results of the vanilla query
            return $query->get();
            
        }
        
        // otherwise...
        else{
            
            // make a new paramHelper...
            $helper = new ParamHelper();
            
            // ..to process them params, modify query, and return results.
            return $helper->processParams($request,$query,'product');
        }
    }
 
    public function show(Product $product)
    {
        return $product;
    }
 
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|max:255',
          'description' => 'required',
          'price' => 'integer',
          'availability' => 'boolean',
        ]);

        $product = Product::create($request->all());
 
        return response()->json($product, 201);
    }
 
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
 
        return response()->json($product, 200);
    }
 
    public function delete(Product $product)
    {
        $product->delete();
 
        return response()->json(null, 204);
    }
 
}
