<?php

namespace App\Http\Controllers;

use App\Condo;
use Illuminate\Http\Request;
use App\ParamHelper;

class CondoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // prepare a vanilla query that returns simple info
        $query = Condo::select('id','name','num_apts','type');
        
        // get the available url params
        $params = $request->query();
        
        // if no url params
        if(empty($params)){
            
            // return all results of the vanilla query
            return $query->get();
            
        }
        
        // otherwise...
        else{
            
            // make a new paramHelper...
            $helper = new ParamHelper();
            
            // ..to process them params, modify query, and return results.
            return $helper->processParams($request,$query,'condo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Condo  $condo
     * @return \Illuminate\Http\Response
     */
    public function show(Condo $condo)
    {
        return response()->json($condo, 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'num_apts' => 'required',
          'type' => 'required',
          'addr_street' => 'required',
          'addr_num' => 'required',
          'addr_city' => 'required',
          'addr_zip' => 'required',
          'addr_muni' => 'required',
          'addr_state' => 'required',
          'addr_country' => 'required',
          'admin_id' => 'required'
        ]);

        $condo = Condo::create($request->all());
 
        $condo->admins()->attach($request->admin_id);
        
        return response()->json($condo, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Condo  $condo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Condo $condo)
    {
        $condo->update($request->all());
 
        return response()->json($condo, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Condo  $condo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Condo $condo)
    {
        $condo->delete();
 
        return response()->json(['success'=> true, 'message'=> 'Condo #'.$condo->id.' has been succesfully deleted'], 200);
    }
}
