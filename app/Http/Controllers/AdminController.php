<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Hash;
use App\ParamHelper;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // prepare a vanilla query that returns simple info
        $query = Admin::select('id','fname','lname','email');
        
        // get the available url params
        $params = $request->query();
        
        // if no url params
        if(empty($params)){
            
            // return all results of the vanilla query
            return $query->get();
            
        }
        
        // otherwise...
        else{
            
            // make a new paramHelper...
            $helper = new ParamHelper();
            
            // ..to process them params, modify query, and return results.
            return $helper->processParams($request,$query,'admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return response()->json($admin, 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'fname' => 'required',
          'lname' => 'required',
          'email' => 'required|unique:admins|email',
          'password' => 'required|alpha_num',
        ]);

        $admin = Admin::create(['fname' => $request->fname, 'lname' => $request->lname, 'email' => $request->email, 'password' => Hash::make($request->password)]);
 
        return response()->json($admin, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $this->validate($request, [
          'email' => 'unique:admins|email',
          ]);
        
        $admin->update($request->all());
 
        return response()->json($admin, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();
 
        return response()->json(['success'=> true, 'message'=> 'Admin #'.$admin->id.' has been succesfully deleted'], 200);
    }
}
