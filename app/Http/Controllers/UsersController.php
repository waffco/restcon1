<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\User;
 
class UsersController extends Controller
{
 
    public function index()
    {
        return User::all();
    }
 
    public function show(User $user)
    {
        return $user;
    }
 
    // Creating a user is handled through register in auth
    /*
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required|max:255',
          'description' => 'required',
          'price' => 'integer',
          'availability' => 'boolean',
        ]);

        $product = Product::create($request->all());
 
        return response()->json($product, 201);
    }

    */
 
    public function update(Request $request, User $user)
    {
        //dd($request->all());
        $user->update($request->all());
 
        return response()->json($user, 200);
    }
 
    public function delete(User $user)
    {
        $user->delete();
 
        return response()->json(['success'=> true, 'message'=> 'User #'.$user->id.' has been succesfully deleted'], 200);
    }
 
}
