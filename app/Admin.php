<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 
        'lname', 
        'email', 
        'password',
        'sex',
        'birthdate',
        'tel1',
        'tel2',
        'addr_street',
        'addr_num_ext',
        'addr_num_int',
        'city',
        'state',
        'zip',
        'newsletter',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * The Condos that are associated to the Admin
     */
    public function condos()
    {
        return $this->belongsToMany(Condo::class);
    }
    
    /**
     * The Condos that are associated to the Admin
     */
    public function residents()
    {
        return $this->hasManyThrough(Resident::class,Condo::class);
    }
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
