<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resident extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'condo_id',
        'fname', 
        'lname', 
        'email',
        'username',
        'password',
        'interior',
        'sex',
        'birthdate',
        'tel1',
        'tel2',
        'newsletter',
        'active',
        'quota',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * The Condos that are associated to the Admin
     */
    public function condos()
    {
        return $this->belongsTo(Condo::class, 'condo_id');
    }
}
