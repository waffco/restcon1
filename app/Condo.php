<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condo extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'num_apts', 
        'type', 
        'addr_street',
        'addr_num',
        'addr_city',
        'addr_zip',
        'addr_muni',
        'addr_state',
        'addr_country',
        'condo_level',
        'rulebook',
        'bank_name',
        'bank_account',
        'bank_clabe',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    
    /**
     * The Admins that are associated to the Condo
     */
    public function admins()
    {
        return $this->belongsToMany(Admin::class);
    }
    
    /**
     * The Residents that reside in the Condo
     */
    public function residents()
    {
        return $this->hasMany(Resident::class);
    }
}
