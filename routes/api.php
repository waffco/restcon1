<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// dummy unsecured routes
Route::get('products', 'ProductsController@index');
Route::get('products/{product}', 'ProductsController@show');
Route::post('products','ProductsController@store');
Route::put('products/{product}','ProductsController@update');
Route::delete('products/{product}', 'ProductsController@delete');

// unsecured registration and recovery endpoints.
Route::post('register', 'AuthController@register');
Route::post('users','AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');

Route::group(['middleware' => ['jwt.auth']], function() {

    Route::get('logout', 'AuthController@logout');
    
    Route::get('sproducts', 'ProductsController@index');
    Route::get('sproducts/{product}', 'ProductsController@show');
    Route::post('sproducts','ProductsController@store');
    Route::put('sproducts/{product}','ProductsController@update');
    Route::delete('sproducts/{product}', 'ProductsController@delete');
    
    Route::get('users', 'UsersController@index');
    Route::get('users/{user}', 'UsersController@show');
    Route::post('users','AuthController@register');
    Route::put('users/{user}', 'UsersController@update');
    Route::delete('users/{user}', 'UsersController@delete');
    
    Route::get('admins', 'AdminController@index');
    Route::get('admins/{admin}', 'AdminController@show');
    Route::post('admins','AdminController@store');
    Route::put('admins/{admin}', 'AdminController@update');
    Route::delete('admins/{admin}', 'AdminController@destroy');

    Route::get('condos', 'CondoController@index');
    Route::get('condos/{condo}', 'CondoController@show');
    Route::post('condos','CondoController@store');
    Route::put('condos/{condo}', 'CondoController@update');
    Route::delete('condos/{condo}', 'CondoController@destroy');

    Route::get('residents', 'ResidentController@index');
    Route::get('residents/{resident}', 'ResidentController@show');
    Route::post('residents','ResidentController@store');
    Route::put('residents/{resident}', 'ResidentController@update');
    Route::delete('residents/{resident}', 'ResidentController@destroy');

});
